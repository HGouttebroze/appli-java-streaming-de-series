# Java SE application

## Concepteur Développeur d’Applications

### Java SE

### Présentation du projet

+ développer une application Java en console permettant de : 
    + gérer des séries
    + leurs saisons 
    + leurs épisodes.

+ Une série comporte plusieurs saisons. 
+ Une saison comporte plusieurs épisodes.

### TODO

+ Dans le package de base, ajouter les packages `business`, `service` et `util`.

+ Écrire les classes métier dans le package `business`, 

    + Classes identiques & à représenter ds le diagramme de classes à générer (format PNG)
+ interfaces et classes de `service` respectivement dans le package `service` et
`service.impl` 

+ méthode qui ajoute deux séries, deux saisons pour chaque série et quatre
épisodes pour chaque saison

+ classe App : en exécutant la méthode main de cette classe, le menu principal suivant s'affiche

+ Lecture de toutes les info sur une série 
    + l’utilisateur saisit l’id de la série

+ Ajout épisode 
    + l’utilisateur choisit la série, par son id), sa saison
    + saisit le nom et la date à laquelle l'épisode a été ajouté

+ Suppretion des épisodes 
    + user choisit la série (par id, saison, id pisode à supprimer)

### TRIE / COMPARAISON

+ séries triées sur le nom
+ séries triées sur le prix
+ épisodes ajoutés entre deux dates

### Doc Tech

+ On génére la javadoc 


+ version 14 JAVA 