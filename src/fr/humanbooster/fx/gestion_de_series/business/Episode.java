package fr.humanbooster.fx.gestion_de_series.business;

import java.util.Date;

public class Episode {
	//Attributs
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private int dureeEnMinutes;
	private Date dateAjout;
	private Saison saison;
	
	//Constructeur
	public Episode(String nom, int dureeEnMinutes, Date dateAjout, Saison saison) {
		super();
		this.id = ++compteur;
		this.nom = nom;
		this.dureeEnMinutes = dureeEnMinutes;
		this.dateAjout = dateAjout;
		this.saison = saison;
		this.saison.getEpisodes().add(this);
	}

	//get & set
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDureeEnMinutes() {
		return dureeEnMinutes;
	}

	public void setDureeEnMinutes(int dureeEnMinutes) {
		this.dureeEnMinutes = dureeEnMinutes;
	}

	public Date getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(Date dateAjout) {
		this.dateAjout = dateAjout;
	}

	public Saison getSaison() {
		return saison;
	}

	public void setSaison(Saison saison) {
		this.saison = saison;
	}

	//toString
	@Override
	public String toString() {
		return "Episode id=" + id + ", nom=" + nom + ", dureeEnMinutes=" + dureeEnMinutes + ", dateAjout=" + dateAjout;
	}
	
}
