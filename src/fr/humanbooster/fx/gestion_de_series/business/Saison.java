package fr.humanbooster.fx.gestion_de_series.business;

import java.util.ArrayList;
import java.util.List;

public class Saison {
	//attributs
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private Serie serie;
	private List<Episode> episodes;
	
	//Constructeur
	public Saison(String nom, Serie serie) {
		super();
		this.id = ++compteur;
		this.nom = nom;
		this.serie = serie;
		this.serie.getSaisons().add(this);
		this.episodes = new ArrayList<>();
	}

	//Get & Set
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public List<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<Episode> episodes) {
		this.episodes = episodes;
	}

	//toString
	@Override
	public String toString() {
		return "Saison [id=" + id + ", nom=" + nom + ", serie=" + serie.getNom() + ", episodes=" + episodes + "]";
	}	
}
