package fr.humanbooster.fx.gestion_de_series.business;

import java.util.ArrayList;
import java.util.List;

public class Serie implements Comparable<Serie>{
	//attributs
	private static Long compteur = 0L;
	private Long id;
	private String nom;
	private float prixEnEuros;
	private List<Saison> saisons;
	
	
	//Constructeur
	public Serie(String nom, float prixEnEuros) {
		super();
		this.id = ++compteur;
		this.nom = nom;
		this.prixEnEuros = prixEnEuros;
		this.saisons = new ArrayList<>();
	}

	//Get & Set
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public float getPrixEnEuros() {
		return prixEnEuros;
	}


	public void setPrixEnEuros(float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}


	public List<Saison> getSaisons() {
		return saisons;
	}


	public void setSaisons(List<Saison> saisons) {
		this.saisons = saisons;
	}

	@Override
	public int compareTo(Serie autreSerie) {
		return getNom().compareTo(autreSerie.getNom());
	}

	@Override
	public String toString() {
		return "Serie [id=" + id + ", nom=" + nom + ", prixEnEuros=" + prixEnEuros + ", saisons=" + saisons + "]";
	}
	
}