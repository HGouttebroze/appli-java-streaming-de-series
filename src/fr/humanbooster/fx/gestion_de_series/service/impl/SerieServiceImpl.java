package fr.humanbooster.fx.gestion_de_series.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Serie;
import fr.humanbooster.fx.gestion_de_series.service.SerieService;
import fr.humanbooster.fx.gestion_de_series.util.ComparateurDeSerieParPrix;

public class SerieServiceImpl implements SerieService {
	
	
	private List<Serie> series = new ArrayList<>();

	
	@Override
	public Serie ajouterSerie(String nom, float prixEnEuros) {
		Serie serie = new Serie(nom, prixEnEuros);
		series.add(serie);
		return serie;
	}

	@Override
	public List<Serie> recupererSeries() {
		return series;
	}

	@Override
	public Serie recupererSerie(Long id) {
		for (Serie serie : series) {
			if (serie.getId().equals(id)) {
				return serie;
			}
		}
		return null;
	}

	@Override
	public void trierSeriesParNom() {
		Collections.sort(series);
		
	}

	@Override
	public void trierSeriesParPrix() {
		Collections.sort(series, new ComparateurDeSerieParPrix());
		
	}

	@Override
	public boolean supprimerSerie(Long idSerie) {
		Serie serieASupprimer = recupererSerie(idSerie);
		if (serieASupprimer==null) {
			return false;
		} else {
			series.remove(serieASupprimer);
			return true;
		}
	}

}
