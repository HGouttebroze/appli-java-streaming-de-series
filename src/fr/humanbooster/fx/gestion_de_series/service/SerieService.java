package fr.humanbooster.fx.gestion_de_series.service;

import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Serie;

public interface SerieService {
	Serie ajouterSerie(String nom, float prixEnEuros);

	List<Serie> recupererSeries();

	Serie recupererSerie(Long id);

	void trierSeriesParNom();

	void trierSeriesParPrix();

	boolean supprimerSerie(Long idSerie);

}