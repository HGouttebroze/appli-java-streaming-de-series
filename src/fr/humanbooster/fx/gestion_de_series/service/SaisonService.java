package fr.humanbooster.fx.gestion_de_series.service;

import java.util.List;

import fr.humanbooster.fx.gestion_de_series.business.Saison;
import fr.humanbooster.fx.gestion_de_series.business.Serie;

public interface SaisonService {
	Saison ajouterSaison(String nom, Serie serie);

	List<Saison> recupererSaisons();

	Saison recupererSaison(Long id);

	List<Saison> recupererSaisons(Serie serie);

}
